// ==========================THEORY================================
/*
Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна.

Деструктуризація являє собою зручний варіант вилучення зачень або властивостей з об'єктів та масивів і подальша маніпуляція з ними, як з окремими елементами. Це є зручним варіантом роботи коли ми зустрічаємося з великим обємом данних і нам потрібно його розпаковувати та обробляти, зєднувати з іншими данними. Це спрощує роботу в кодуванні та маніпуляції з масивами та обєктами.
*/


// =========================PRACTICE===============================

// 1.

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const [...clients] = clients1.concat(clients2);

const newArrClients = clients.filter((value, index, self) => {
    return self.indexOf(value) === index;
})

console.log(newArrClients);


// 2. 

const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];

  const charactersShortInfo = characters.map(({name, lastName, age}) =>
   ({name, lastName, age}));

  console.log(charactersShortInfo);

// 3.

const user1 = {
    name: "John",
    years: 30
  };

  let {name: name, years: years, isAdmin = false} = user1;

  console.log(name, years, isAdmin);


  // 4.

  const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }


  let info2018;
  let info2019;
  let info2020;


 let fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};

 console.log(fullProfile);
  

// 5.

const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
  }

  let fullBooks = [...books, bookToAdd];

  console.log(fullBooks);


  // 6.

  const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }
  
  let age = 30;
  let salary = 2000;
  let newEmployee = {...employee, age, salary};

  console.log(newEmployee);


// 7.

const array = ['value', () => 'showValue'];

let [value] = array;
let [, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'
